+++
author = ""
bg_image = "/images/apertura.jpg"
categories = []
date = 2022-03-10T03:00:00Z
description = ""
image = "/images/apertura-1.jpg"
tags = []
title = "SE REALIZÓ LA APERTURA DE SESIONES ORDINARIAS DEL CONCEJO DELIBERANTE DE TIGRE"
type = "post"

+++
_“El presidente del Concejo Deliberante Segundo Cernadas dio inicio a la sesión especial invitando a los presentes a entonar las estrofas del himno nacional argentino y luego de dar la bienvenida al intendente y los secretarios del municipio, invito a Julio Zamora a dar su discurso para dejar abierto el periodo de sesiones ordinarias 2022”._

En su discurso Zamora hizo un repaso de su gestión y marcó los lineamientos básicos y obras planificadas para este nuevo año. Además, agradeció a los concejales por el trabajo legislativo de 2021 y fundamentalmente por el tratamiento de ordenanzas tan importantes como el presupuesto y la ordenanza fiscal.

Finalmente agradeció la gran concurrencia e invito a trabajar al departamento ejecutivo, a los concejales y a los vecinos para hacer de Tigre el mejor municipio de la provincia.