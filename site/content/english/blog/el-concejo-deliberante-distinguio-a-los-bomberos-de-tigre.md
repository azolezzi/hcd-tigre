+++
author = ""
bg_image = "/images/whatsapp-image-2022-03-07-at-14-28-36.jpeg"
categories = []
date = 2022-03-07T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-03-07-at-14-28-36.jpeg"
tags = []
title = "EL CONCEJO DELIBERANTE DISTINGUIÓ A LOS BOMBEROS DE TIGRE"
type = "post"

+++
**El presidente de Honorable Concejo Deliberante de Tigre Segundo Cernadas visitó el cuartel central de Bomberos voluntarios de Tigre para entregar un reconocimiento por la tarea realizada durante los incendios en la provincia de Corrientes.**

En el encuentro dialogaron sobre las acciones realizadas por el personal para contribuir en el momento más difícil de los de los incendios en el norte del país.

“Desde el Concejo Deliberante queríamos reconocer el enorme trabajo que realizaron nuestros bomberos en estos incendios de la provincia de Corrientes, pero también destacar la labor que hacen en nuestra comunidad y el importante compromiso que demuestran cada día” destacó Segundo Cernadas.

Por su parte las autoridades de la institución agradecieron reconocimiento del Concejo Deliberante por la tarea desarrollada para sofocar el incendio forestal.