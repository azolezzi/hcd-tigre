+++
author = ""
bg_image = "/images/img_1703.jpg"
categories = []
date = 2021-05-05T03:00:00Z
description = ""
image = "/images/img_1703.jpg"
tags = []
title = "EL CONCEJO DELIBERANTE DE TIGRE APROBÓ 11 PROYECTOS Y COMENZÓ A TRATAR OTROS 31 SOBRE EDUCACIÓN, SEGURIDAD Y SALUD PÚBLICA"
type = "post"

+++

**_Los concejales de Tigre se reunieron este martes. Además de los proyectos aprobados se hicieron homenajes vinculados al día del trabajador, a los vecinos víctimas del COVID-19 y a un nuevo aniversario del hundimiento del Crucero General Belgrano en 1982. Asumió su banca el Concejal Francisco Rosso_**

Diversos proyectos como la declaración de “Interés Municipal” en la transformación de la Escuela de Educación secundaria N.º 1 en Escuela Especializada en Arte, la colocación de cámaras de seguridad en la Ciudad de Don Torcuato y la creación de un Centro Municipal de Diagnóstico, Tratamiento e Inclusión socio educativa para la atención de menores de 18 años, fueron solo algunos de los tantos proyectos tratados esta mañana en el Concejo Deliberante de Tigre.

Con la presencia de 20 Concejales y Concejalas, sobre un total de 24, este martes 4 de mayo se llevó a cabo la 3° Sesión Ordinaria del Honorable Concejo Deliberante de Tigre. La misma fue de manera presencial y respetando los protocolos sanitarios establecidos.

Hubo un sentido homenaje a los familiares y vecinos de Tigre que fallecieron desde el comienzo de la Pandemia, producto del virus SARS-CoV-2. A su vez, los concejales hicieron mención al Día del Trabajador (1 de mayo), al Día Día Mundial de la Libertad de Prensa (3 de mayo) y brindaron sus respetos al Cabo 2° Rogelio Juan Benitez y al Marinero Raúl Nuñez, 2 vecinos de Tigre que perdieron su vida durante el hundimiento del ARA General Belgrano el 2 de mayo de 1982 durante el Conflicto del Atlántico Sur.

En esta primera sesión ordinaria del año fueron aprobados más de 11 proyectos e ingresaron 31 nuevos expedientes, que fueron girados a comisión para su posterior tratamiento durante los próximos días.

ALGUNOS DE LOS PROYECTOS APROBADOS

Se solicitó al ejecutivo municipal la colocación de cámaras de seguridad en Balbastro entre Chile y Chacabuco en la Ciudad de Don Torcuato.

Se declaró de “Interés Municipal” la transformación de la Escuela de Educación Secundaria N.º 1 en Escuela Especializada en Arte.

También, se declaró el 14 de marzo de cada año como el “Día Municipal de la Endometriosis” y el “Marzo Amarillo”.

 Otro de los proyectos presentados pretende la creación de un Centro Municipal de Diagnóstico, Tratamiento e Inclusión socio educativa para la atención a menores de 18 años y para sus familias.

DESTACADOS

El Concejal Francisco Rosso volvió a integrar el cuerpo y en su rol de vocal ocupará las Comisiones de “Legislación, interpretación y reglamento” y de “Obras Públicas” en lugar del Concejal Daniel Gambino y la Comisión de “Derechos Humanos” en lugar de la Concejala Sandra Rossi.