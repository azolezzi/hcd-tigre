+++
author = ""
bg_image = "/images/marcelo-carpita-muralista.jpeg"
categories = []
date = 2022-03-02T03:00:00Z
description = ""
image = "/images/marcelo-carpita-muralista.jpeg"
tags = []
title = "EL HCD REALIZA OBRAS DE RESTAURACIÓN DE SU PATRIMONIO CULTURAL"
type = "post"

+++
###### **Marcelo Carpita es docente, muralista, productor de arte público y fue uno de los artistas que participaron del diseño del mural de nuestro recinto Gral. San Martín. Ahora con la ayuda de sus colegas Gerardo Cianciolo, Leopoldo Ortiz y Enrique García, están trabajando en la puesta de valor de nuestro reconocido mural.**

> Por su parte, el presidente del Concejo Deliberante, Segundo Cernadas declaró “El Concejo Deliberante de Tigre estará siempre abierto a los vecinos y tenemos la responsabilidad de conservar su patrimonio para que lo puedan disfrutar”

###### Entrevista con Marcelo Carpita

**¿Cómo surgió la idea para pintar el mural del Concejo Deliberante en 1997?**

Nos habían convocado para realizar un diseño para el salón allá por el 97´ y habíamos pensado en reflejar de cierta manera un mural que hablara sobre el Tigre productivo e industrial, el Tigre continental.

**¿Qué buscaban representar a través de este mural?**

En aquel momento pensamos más un tigre de la economía popular, con un tren que logró llegar a pedido de la gente, el ceibo que es nuestra flor nacional y representar también a nuestro Delta como ese espacio de distención con la gente pescando sobre la orilla. En su momento decidimos utilizar colores tierra, más bien neutros para que no resultara tan invasivo y generaran un clima especial con cierta calidez.

Lo de ahora es una puesta en valor con iluminaciones más fuertes en algunos lados y la pintura que hicimos en aquel momento ya no es la misma.

**¿Qué diferencias crees que hay entre aquellos artistas que eran en el 97´con respecto a los artistas que son en la actualidad?**

Uno ya no dibuja como en aquella época. Cuando nos toca poner en valor alguna obra, no la podemos ver de la misma manera. Si bien buscamos mantener el espíritu de la obra, noto ciertas debilidades en el mural que ideamos en aquel entonces y que hoy día estamos buscando fortalecer a través de esta puesta en valor.

**¿Y si tuvieses que hacer un nuevo mural de Tigre, habiendo transcurrido 25 años, cambiarías algo o volverías a representarlo de la misma manera?**

Si tuviese que realizar un nuevo mural, seguramente volvería a hablar con la gente de Tigre porque los paradigmas cambian y las formas de pensar también. Probablemente haría mayor énfasis en la participación de la mujer en nuestra sociedad. Por ejemplo, en el mural original habíamos colocado 2 mujeres y 6 hombres y estoy convencido que hoy día buscaríamos algo mucho más equitativo y representativo. En aquel momento la sociedad tenía ese preconcepto de relacionar la industria con la masculinidad y hoy día esos paradigmas cambiaron.

También cambiaría algunas formas y utilizaría elementos como el esténcil y algunas técnicas más modernas.