+++
author = ""
bg_image = "/images/hcd-1-sesion.jpeg"
categories = []
date = 2022-03-24T03:00:00Z
description = ""
image = "/images/hcd-1-sesion.jpeg"
tags = []
title = "EN LA PRIMERA SESIÓN DEL AÑO EL HCD TIGRE TRATO MÁS DE 50 EXPEDIENTES"
type = "post"

+++
_En la tercera reunión, primera sesión ordinaria del año 2022, se ingresaron veintiséis nuevos proyectos para mejorar la vida de los vecinos de Tigre y se le dio tratamiento a cincuenta y dos expedientes que se trabajaron previamente en las comisiones del Concejo Deliberante._

Antes del tratamiento de los proyectos, en la media hora de consultas y homenajes, la concejala Verónica Caamaño (Bloque Frente de Todos) realizó un homenaje en ocasión de celebrarse la semana de la memoria, nombrando a todos los detenidos desaparecidos de Tigre.

A su tiempo el concejal de la U.C.R Lisandro Lanzón (Bloque Juntos) hizo mención a la noche más oscura que vivió la Argentina, rescató el coraje del ex presidente Raúl Alfonsín que juzgó a las juntas militares y homenajeo a todos los dirigentes que durante estos años han mantenido vigente el sistema democrático.

El concejal Adolfo Leber (Bloque Juntos) finalmente, hizo un homenaje al comerciante local y reconocido vecino de la zona Jorge Raúl Fava, recientemente fallecido.

Respecto a los proyectos ingresados y girados a comisiones se destacan la instalación de cámaras de seguridad en Tigre Centro y la instalación de un nuevo semáforo en la localidad de El Talar, entre otros de enorme relevancia para los vecinos de Tigre.

Entre los proyectos aprobados de mayor importancia se encuentran el de construcción de un refugio para peatones y construcción de veredas en la localidad de Rincón de Milberg. Otro que dispone la instalación de una cámara seguridad en la calle Chacabuco de Tigre Centro; el proyecto aprobando una campaña de prevención contra la violencia de genero con una señal de ayuda y finalmente el expediente declarando de interés municipal las actividades de la 40º conmemoración de la máxima resistencia por la Gesta de Malvinas.