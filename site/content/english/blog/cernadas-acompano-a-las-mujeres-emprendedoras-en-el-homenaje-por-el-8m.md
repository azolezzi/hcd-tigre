+++
author = ""
bg_image = "/images/dia-de-la-mujer-hcd.jpeg"
categories = []
date = 2022-03-08T03:00:00Z
description = ""
image = "/images/dia-de-la-mujer-hcd.jpeg"
tags = []
title = "CERNADAS ACOMPAÑÓ A LAS MUJERES EMPRENDEDORAS EN EL HOMENAJE POR EL 8M"
type = "post"

+++
_El Concejo Deliberante de Tigre reconoció el trabajo de emprendedoras locales como parte de las actividades realizadas en el día internacional de la mujer._

Durante la jornada del 8M por el día internacional de la mujer Segundo Cernadas acompañó las actividades de homenaje reclamando el reconocimiento del rol de la mujer: “Donde están ellas, el trabajo es mejor, es más eficiente, es más honesto y más comprometido”.

Tras la realización de un taller de para emprendedoras donde se dio capacitación en redes sociales y registro de marca en los Jardines del Museo de Arte de Tigre, Cernadas afirmó que “más mujeres es mejor trabajo”.

El presidente del Concejo Deliberante local, felicitó la iniciativa y destacó “Es increíble la energía que tienen las mujeres que deciden emprender, ponen el corazón para sacar sus familias adelante, se capacitan, son solidarias y nos demuestran que tenemos un gran futuro en Tigre. Realmente son merecedoras de este homenaje que les hacemos desde el Concejo Deliberante”.

El encuentro reunió a unas cincuenta mujeres emprendedoras del distrito que escucharon atentamente a las oradoras Tini Lecomte de “blog de Ideas” y a Gini Terrile de “Trade Mark”, ambas contaron sus experiencias al emprender y les compartieron tips para hacer crecer sus negocios desde las redes sociales, el registro de su marca o el diseño de un logo que sea apropiado.

“Hace tiempo venimos trabajando en talleres de oficios y en brindar herramientas para que las mujeres puedan empoderarse, sentimos que una mejor argentina se construye desde el trabajo y la educación, es por eso que este 8M decidimos celebrar junto a las mujeres que se animan y no se dan por vencidas” indicó la concejal Sofía Bravo.

Luego de la actividad principal se realizó un homenaje y abrió un espacio de intercambio entre los asistentes del que también participaron las concejalas Ana María Fernández Costa, Marcela Cesare, Ximena Pereyra, Josefina Pondé y los concejales Mariano Pelayo, Juan Furnari, Adolfo Leber, Lisandro Lanzón y Nicolas Massot.