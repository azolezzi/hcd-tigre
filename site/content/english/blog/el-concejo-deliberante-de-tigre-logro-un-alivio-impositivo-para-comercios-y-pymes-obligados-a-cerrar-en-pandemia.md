+++
author = ""
bg_image = "/images/img_1895.JPG"
categories = []
date = 2021-05-28T03:00:00Z
description = ""
image = "/images/img_1895.JPG"
tags = []
title = "EL CONCEJO DELIBERANTE DE TIGRE LOGRÓ UN ALIVIO IMPOSITIVO PARA COMERCIOS Y PYMES OBLIGADOS A CERRAR EN PANDEMIA"
type = "post"

+++

**_El Concejo Deliberante de Tigre aprobó la ordenanza que lleva un alivio fiscal para quienes no pueden trabajar en pandemia._**

El Concejo Deliberante de Tigre aprobó por unanimidad la ordenanza gracias al trabajo en conjunto de todos los bloques. La medida permitirá que los comerciantes, las Pymes y emprendedores sean eximidos de las tasas que gravan actividades que fueron afectadas por las restricciones de presencialidad. Además se consideró un apoyo al transporte fluvial cuyo funcionamiento se vio seriamente afectado.

Especialmente se tuvo en cuenta la situación de los rubros más afectados y se dispuso para ellos, además, la eximición de los anticipos de abril, mayo y junio de la Tasa por Verificación y de la Tasa por Publicidad para los rubros gastronómicos, de eventos, servicios de organización, dirección y gestión de prácticas deportivas, establecimientos educativos donde solo funcione nivel inicial, jardines maternales y guarderías infantiles.

La medida, que deberá ser implementada por el municipio, se tomó en el contexto del reclamo por la existencia de fondos municipales destinados a las partidas con fondos afectados.

Desde el Concejo advirtieron que esta medida aprobada por la ordenanza busca dar respuesta a quienes sostienen el funcionamiento del Municipio con sus impuestos y reclaman mayor atención a la hora de establecer prioridades desde el Estado.