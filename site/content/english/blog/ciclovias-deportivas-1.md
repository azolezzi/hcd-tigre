+++
author = ""
bg_image = ""
categories = []
date = 2021-08-12T03:00:00Z
description = ""
image = "/images/whatsapp-image-2021-08-12-at-08-52-37.jpeg"
tags = []
title = "CICLOVÍAS DEPORTIVAS"
type = "post"

+++
🚲¡Seguimos avanzando por un Tigre en bicicleta! En el día de hoy nos reunimos concejales y concejalas de nuestro espacio junto a Daniela Donadio y otros ciclistas, para avanzar con el proyecto de ciclovías deportivas

La propuesta que impulsamos considera la utilización del Camino de los Remeros para práctica deportiva y recreativa del ciclismo los domingos de 7 a 11. Esto va a posibilitar mejorar la situación del espacio junto al río, y la práctica de deportes. De esta manera buscamos fomentar el ciclismo y también un mejor ordenamiento del tránsito, descomprimiendo el flujo de ciclistas en Villanueva y brindando mayor seguridad para todos y todas

Este es un paso más hacia la ciudad moderna que nos merecemos. Una ciudad amigable, ecológica y sustentable.