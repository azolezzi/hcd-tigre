+++
author = ""
bg_image = "/images/img_0578-01.jpeg"
categories = []
date = 2021-03-09T14:00:00Z
description = ""
image = "/images/img_0578-01.jpeg"
tags = []
title = "JULIO ZAMORA JUNTO A SEGUNDO CERNADAS DIERON INICIO A LAS SESIONES EN EL CONCEJO DELIBERANTE DE TIGRE"
type = "post"

+++

**_El Honorable Concejo Deliberante de Tigre dio inicio a lo que será un año de tratamiento de diversos proyectos y cuestiones que incumben a los ciudadanos del partido de Tigre._**

Esta mañana en el Salón del Concejo Deliberante de Tigre se llevó a cabo la Ceremonia de Apertura del Período de Sesiones Ordinarias 2021. El Intendente de Tigre, Julio Zamora, dio comienzo a la jornada con un discurso inaugural, donde repasó los logros de su gestión, así cómo también los nuevos desafíos para el año entrante.

El inicio de la sesión se llevó a cabo a las 11 hs en el Salón del Concejo Deliberante de Tigre, ante la presencia de las autoridades del Concejo, los concejales y los secretarios del Ejecutivo Municipal.

Con una breve introducción, dio comienzo a la sesión el Presidente del Concejo Deliberante, Segundo Cernadas, quien invitó al intendente Julio Zamora a subir al estrado para comenzar su discurso de inauguración. Zamora, repasó a lo largo de su presentación los logros obtenidos durante estos años de gestión y los desafíos que pretende realizar a lo largo del 2021.

Podes ver la sesión completa haciendo clic en este link: [https://www.youtube.com/watch?v=VHyuQGbhDM8&t=126s](https://www.youtube.com/watch?v=VHyuQGbhDM8&t=126s "https://www.youtube.com/watch?v=VHyuQGbhDM8&t=126s")

##### AUTORIDADES PRESENTES DURANTE LA SESIÓN:

* Julio Zamora (Intendente)
* Segundo Cernadas (Presidente del Concejo Deliberante)
* Pedro Heyde (Secretario General de Obras y Servicios Públicos)
* Mario Zamora (Secretario de Gobierno)
* Aníbal Mastroianni (Secretario de Economía y Hacienda)
* Fernando Abramzon (Secretario de Salud)
* Cecilia Ferreira (Secretaria de Desarrollo Social y Políticas de Inclusión)
* Emiliano Mansilla (Secretario de Desarrollo Económico y Relaciones con la Comunidad)
* Ximena Guzman (Secretaria de Protección Ciudadana)
* Florencia Mosqueda (Secretaria de Turismo)
* Ignacio Castro Cranwell (Secretario de Comunicación)
* Padre Cote Quijano
* Raúl Botelli (Secretario del HCD)
* Daniel Gambino (Vicepresidente del HCD)
* Verónica Caamaño (Vicepresidenta 2° del HCD)
* Ana María Fernándes Costa (Prosecretaria del HCD)
* Camila Benedict (Concejala)
* Carla Kammann (Concejala)
* Gisela Zamora (Concejala)
* Gladys Pollan (Concejala)
* Javier Forlenza (Concejal)
* Javier Parbst (Concejal)
* Juan Furnari (Concejal)
* Lucas Gianella (Concejal)
* Marcos Tenaglia (Concejal)
* Mariano Pelayo (Concejal)
* Mayra Mariani (Concejala)
* Rodrigo Alvarez (Concejal)
* Rodrigo Molino (Concejal)
* Sandra Rossi (Concejala)
* Teresa Paunovich (Concejala)