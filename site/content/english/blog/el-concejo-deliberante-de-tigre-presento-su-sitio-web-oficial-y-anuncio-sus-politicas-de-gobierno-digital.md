+++
author = ""
bg_image = "/images/whatsapp-image-2021-03-02-at-09-56-53.jpeg"
categories = []
date = 2021-03-02T12:00:00Z
description = ""
image = "/images/whatsapp-image-2021-03-02-at-09-56-53.jpeg"
tags = []
title = "EL CONCEJO DELIBERANTE DE TIGRE PRESENTÓ SU SITIO WEB OFICIAL Y ANUNCIÓ SUS POLÍTICAS DE GOBIERNO DIGITAL"
type = "post"

+++

**_Junto a concejales y autoridades, se presentó el sitio web oficial del Concejo Deliberante de Tigre y anunció que se trabaja en la digitalización administrativa, una mejor conectividad y la transmisión en línea de las sesiones del cuerpo legislativo._**

El concejo Deliberante de Tigre lanzó su sitio web y cubrió una demanda de información, apertura y transparencia con la que no contaba en la actualidad. Desde ahora estarán en línea todas las órdenes del día con los proyectos presentados y todas las actas con los que resultaron aprobados. Además estará disponible la información de quienes son los concejales que integran el cuerpo, las comisiones en las que se distribuye el trabajo legislativo y se podrá consultar el reglamento interno de funcionamiento del cuerpo.

“El trabajo de actualizar y mejorar el funcionamiento del cuerpo es una tarea compartida por todos los espacios políticos. Lo hacemos con la idea de poner siempre por delante el interés de los vecinos y abrirles el concejo” dijo el presidente del Concejo Deliberante de Tigre Segundo Cernadas. “El espacio para dar a conocer nuestro trabajo en una página web fue presentado la Concejala Sofía Bravo y el proyecto para mejorar la conectividad es de Javier Forlenza. Los dos son de espacios políticos distintos” concluyó Cernadas.

“Notamos que era muy difícil y engorroso para los vecinos de Tigre acceder a los proyectos que presentamos y trabajamos  en el Concejo. Frente a esa necesidad decidimos impulsar un proyecto con el objetivo de desarrollar un sitio web, en el que cualquier vecino pueda conocer de manera automática y transparente, los temas que venimos desarrollando los concejales. Creemos en un gobierno abierto, transparente y cercano al vecino, y este es un paso muy importante para lograrlo”, destacó la Concejala Bravo durante el encuentro.

En la antesala del recinto de sesiones estuvieron el Presidente del Concejo Deliberante, Segundo Cernadas, el Secretario del Concejo, Dr Raúl Eduardo Botelli, la Prosecretaria Ana María Ramos Fernándes Costa y concejales invitados.

“Vamos camino a digitalizar todo y a través de este tipo de acciones estamos buscando modernizar y agilizar los trámites y el acceso a la información del Concejo”, destacó Cernadas durante la presentación del Sitio Web y agregó “Más allá de las diferencias políticas e ideológicas que podamos tener entre los concejales, vamos a seguir trabajando en conjunto para mejorar día a día la vida de todos los tigrenses”.

La página web cuenta además con un apartado de noticias y un sitio para cada concejal en el que cada edil podrá publicar sus propios contenidos legislativos y datos de contacto en rrss, email o teléfono.

Hace clic en el siguiente link para conocer el Sitio: [https://www.hcdtigre.gov.ar/](https://www.hcdtigre.gov.ar/ "https://www.hcdtigre.gov.ar/")