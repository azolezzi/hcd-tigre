---
title: "Noticias"
draft: false
# page title background image
bg_image: "images/backgrounds/Edificio-2.jpg"
# meta description
description : "En esta sección encontrarás las últimas novedades, actividades y eventos realizados por los concejales del HCD."
---