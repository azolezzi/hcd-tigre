---
title: "Atención al Vecino"
draft: false
# page title background image
bg_image: "images/backgrounds/panoramatigre.webp"
# about image
image: "images/about/about-page.jpg"
# meta description
description : "En este portal podrás subir los reclamos, solicitudes e iniciativas ciudadanas que serán gestionadas por el Concejo."

video_link : "https://www.youtube.com/watch?v=wHn1_QVoXGM"
---

VIdeo