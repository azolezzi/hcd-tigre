---
title: "Salud"
draft: false
# page title background image
bg_image: "images/Comisiones/Hospital-tigre.jpg"
# meta description
description : "Completá tus datos para iniciar tu solicitud "
# notice download link
download_link : ""
form: "https://formspree.io/f/xvovwaza"
# type
type: "reclamos"
#procesos
tipos:
    - tipo : "Problemas con la atención en centros municipales"

    - tipo : "Problemas con la antención en centros provinciales"

    - tipo : "Otros"
---


### Salud