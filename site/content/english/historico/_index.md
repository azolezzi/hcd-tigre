---
title: "Historico"
draft: false
# page title background image
bg_image: "images/Comisiones/municip-antigua.jpg"
# meta description
description : "Conocé más sobre la historia del partido de Tigre"

title2: "Un partido con mucha historia"
img: "images/Municip.antigua.jpg"
---

El 8 de julio de 1954 la Legislatura de la Provincia de Buenos Aires sancionó la Ley N° 5.759, en donde se decidió reemplazar el nombre de “Partido de las Conchas” por “Partido de Tigre” o “Ciudad de Tigre”, como se lo conoce popularmente. 

El antiguo “Río de Las Conchas” es conocido hoy como el “Río de la Reconquista”. Cuando hablamos de los orígenes de Tigre, es imposible dejar de mencionar que la línea férrea que conecta la Estación Retiro con la Estación Tigre fue inaugurada en el mes de enero del año 1865.

El 16 de marzo de 1886 se dictó la Ley Orgánica Municipal. Con la aprobación de esta Ley se desdoblaron las funciones de Presidente Municipal y Juez de Paz, es decir, que comenzaron a regir como dos cargos diferentes. 

Dejó de llamarse Presidente Municipal para empezar a ser denominado Intendente Municipal y comenzó a ser la cabeza del Departamento Ejecutivo, secundado por secretarios de las distintas áreas y por un Departamento Deliberativo, integrado por Concejales municipales electos directamente por los vecinos.

El 13 de julio de 1886 fue nombrado Manuel Bird como primer Intendente Municipal. El historiador Enrique de Udaondo afirmó que el Intendente “desempeñó el cargo con acierto y honradez, siendo un funcionario progresista”. 

El cargo de Intendente es elegido de forma directa por los ciudadanos y es por un período de cuatro años. 
Hasta el año 1976 el Honorable Concejo Deliberante de Tigre funcionó en el primer piso del Palacio Municipal. A partir del retorno de la democracia, desde1983 hasta 1996, mudaron sus oficinas  al actual Museo de Arte de Tigre. 

El Honorable Concejo Deliberante de Tigre tuvo los siguientes presidentes desde 1983 hasta el día de la fecha:
Desde 1983 hasta 1985 fue Francisco Ignacio Albisu el presidente del Concejo. Formaba parte de la Unión Cívica Radical.

Desde 1985 hasta 1987 el presidente fue Francisco Blefari. También formaba parte de la Unión Cívica Radical.

Desde 1987 hasta 1988 por el partido Acción Comunal fue presidente Lelio Armando de Pamphilis.

Desde 1988 hasta 2005 fue presidente Hiram Antonio Gualdoni, el cual formaba parte del partido Acción Comunal.

Desde 2005 hasta 2007 también por el partido Acción Comunal fue presidenta del Concejo Celia Nélida Geromel.

Desde 2007 hasta 2009 fue presidente Carlos Daniel Gambino, del Frente Para La Victoria. 

Desde 2009 hasta 2011 Julio Zamora fue presidente del Concejo por el partido Frente Todos por Tigre, y desde 2011 hasta el 2013 por el partido Frente Renovador. 

Desde 2013 hasta 2020 fue la presidenta María Alejandra Nardi, también por el Frente Renovador.

En el año 2020 asumió como Presidente del Honorable Concejo Deliberante de Tigre Segundo Cernadas, por el partido Juntos por el Cambio.
