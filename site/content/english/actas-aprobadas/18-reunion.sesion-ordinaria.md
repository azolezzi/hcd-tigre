---
title: "18ª REUNIÓN – 10ª SESIÓN ORDINARIA"
tipo: "actas"
download_link: "../archivos/actas-aprobadas/18R-10Ordinaria-27-10-2020.docx"
link: "https://docs.google.com/document/d/e/2PACX-1vTQ1XWze13v3b_BmS7rtvyDj-083FmHVN-_FzN0KNjZRVACX-NdgD1MkQXnqxTEPA/pub?embedded=true"
date: 2020-10-27T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/edificio4.jpg"
# meta description
description : ""
# post thumbnail
image: "images/blog/post-1.jpg"
# type
type: "notice"

---
### 18ª REUNIÓN – 10ª SESIÓN ORDINARIA

<iframe style="width: 1000px; height: 700px" src="https://docs.google.com/document/d/e/2PACX-1vSzSyNjUq-EaiZfcmEVJ3Wh7u3bNtnVnbBi9d7e542aQAu9dppzGJDRkHseS_SJ1Q/pub?embedded=true"></iframe>