---
title: "19ª REUNIÓN – 5ª SESIÓN PREPARATORIA"
tipo: "actas"
download_link: "../archivos/actas-aprobadas/19-5Preparatoria-27-10-2020.docx"
link: "https://docs.google.com/document/d/e/2PACX-1vTQ1XWze13v3b_BmS7rtvyDj-083FmHVN-_FzN0KNjZRVACX-NdgD1MkQXnqxTEPA/pub?embedded=true"
date: 2020-10-27T17:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/edificio4.jpg"
# meta description
description : ""
# post thumbnail
image: "images/blog/post-1.jpg"
# type
type: "notice"

---
### 19ª REUNIÓN – 5ª SESIÓN PREPARATORIA

<iframe style="width: 1000px; height: 700px" src="https://docs.google.com/document/d/e/2PACX-1vQoLOxcR6nDstBgx4IZ9F8N9UgmCZNu4Lty_tTDEKZ65Hx9GqJTL8bouNsJOXj0gA/pub?embedded=true"></iframe>