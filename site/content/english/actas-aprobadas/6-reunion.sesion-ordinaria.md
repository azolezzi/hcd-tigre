---
title: "6ª REUNIÓN – 2ª SESIÓN ORDINARIA"
tipo: "actas"
download_link: "../archivos/actas-aprobadas/6-R-2-Ordinaria-23-6-2020.docx"
link: "https://docs.google.com/document/d/e/2PACX-1vTQ1XWze13v3b_BmS7rtvyDj-083FmHVN-_FzN0KNjZRVACX-NdgD1MkQXnqxTEPA/pub?embedded=true"
date: 2020-06-23T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/edificio4.jpg"
# meta description
description : ""
# post thumbnail
image: "images/blog/post-1.jpg"
# type
type: "notice"

---
### 6ª REUNIÓN – 2ª SESIÓN ORDINARIA

<iframe style="width: 1000px; height: 700px" src="https://docs.google.com/document/d/e/2PACX-1vSQ7kIF7RoUUHX3l-QFf1vO3W63vJpEWkHJpFk8oCSbw1C8Xl1-vyBFrLmM2LOddw/pub?embedded=true"></iframe>