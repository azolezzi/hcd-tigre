---
title: "14ª REUNIÓN – 7ª SESIÓN ORDINARIA"
tipo: "actas"
download_link: "../archivos/actas-aprobadas/14R-7-Ordinaria-8-9-2020.docx"
link: "https://docs.google.com/document/d/e/2PACX-1vTQ1XWze13v3b_BmS7rtvyDj-083FmHVN-_FzN0KNjZRVACX-NdgD1MkQXnqxTEPA/pub?embedded=true"
date: 2020-09-08T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/edificio4.jpg"
# meta description
description : ""
# post thumbnail
image: "images/blog/post-1.jpg"
# type
type: "notice"

---
### 14ª REUNIÓN – 7ª SESIÓN ORDINARIA

<iframe style="width: 1000px; height: 700px" src="https://docs.google.com/document/d/e/2PACX-1vQwc8VMCTe2ivNd6tfc8Xmaf5Fr8Zfb5bD-QjmPuRtvUvXX2CRMSEayjSHdy1gvUA/pub?embedded=true"></iframe>