+++
bg_image = "/images/24s-marchajpg.jpg"
concejales = "Leonardo Güi"
date = 2021-12-21T03:00:00Z
description = ""
image = "/images/24s-marchajpg-1.jpg"
title = "MUNICIPIOS Y CAMBIO CLIMÁTICO"
type = ""

+++
Escrito por el Doctor Leonardo Güi

##### **_La falta de Autonomía Municipal en la Provincia de Buenos Aires, hace que la aplicación práctica de la Ley esté sujeta a disposiciones Nacionales, y a su vez las que tome el Gobierno de la Provincia de Buenos Aires. Para el régimen municipal, será clave el trabajo de los entes provinciales encargados en la materia._**

En la última sesión ordinaria del Congreso Nacional del año 2019, se aprobó la Ley N° 27.520, de Presupuestos Mínimos de Adaptación y Mitigación al Cambio Climático Global, y también se dio rango de Ley al Gabinete Nacional de Cambio Climático (que ya había sido creado por decreto en el año 2016), pero siempre sujeto a la controversia que genera la creación de instituciones por Decreto, cuando el instrumento más idóneo para el funcionamiento institucional a largo plazo, es la Ley.

La ley presenta un conjunto bien instrumentado de políticas públicas, en coordinación con las provincias, para trabajar de manera eficiente y a largo plazo sobre la problemática del cambio climático global.

El Decreto Reglamentario Nº 1030/2020 establece las definiciones prácticas sobre los elementos generales que describe la Ley. Dos de sus definiciones más importantes son lo que se entiende por adaptación y mitigación, describiendo en su Art. 3º estos conceptos:

“Adaptación: Ajustes en sistemas naturales y humanos en respuesta a estímulos climáticos actuales o esperados, minimizando el riesgo de daño o aprovechando las oportunidades beneficiosas. Mitigación: Intervención antropógena para reducir las emisiones de fuentes de gases de efecto invernadero, aumentar sus sumideros de dióxido de carbono o destruir otros gases de efecto invernadero”

Desde el punto de vista provincial y municipal, estamos siempre sujetos al poco margen de maniobra que tienen los municipios con respecto a las leyes nacionales de orden público (Art.5 de la ley). La falta de Autonomía Municipal en la Provincia de Buenos Aires, hace que la aplicación práctica de la Ley esté sujeta a disposiciones Nacionales, y a su vez las que tome el Gobierno de la Provincia de Buenos Aires. Para el régimen municipal, será clave el trabajo de los entes provinciales encargados en la materia.

El Art.6 dispone que las autoridades de aplicación local de la ley Nacional, son los organismos que las provincias y CABA determinen para actuar en el ámbito de sus respectivas jurisdicciones. En la Provincia de Buenos Aires, el ente naturalmente encargado debería ser: el OPDS (Organismo Provincial para el Desarrollo Sostenible).

Desde una perspectiva subjetiva, implementar una legislación tan importante para la vida pública municipal, fundamentalmente en los distritos con gran biodiversidad, (como los municipios del Norte del AMBA), debería ser aplicada mediante una nueva creación institucional, un órgano que instrumente la Ley nacional en estrecha relación a las circunstancias particulares de los 135 municipios bonaerenses, enfocándose en modelos de tratamiento específicos, adaptados a las industrias, poblaciones y economías de una provincia de gran diversidad biológica, geográfica y ecológica.

Esta nueva legislación representa un cambio paradigmático en el abordaje jurídico del cambio climático, siendo uno de sus mayores logros, el reconocimiento fáctico del fenómeno, que ya no pasa a ser una cuestión de debate entre políticos y ecologistas, sino, un problema real, que requiere políticas reales.

El interior bonaerense cuenta con municipios que van desde los 5.000 habitantes en el centro oeste, a los (aproximados) dos millones de La Matanza en el área metropolitana, por este motivo, resulta conceptualmente interesante la creación de un ente específico de aplicación que trabaje codo a codo con los municipios y sus realidades.

También resultaría beneficioso, que, dentro de las posibilidades que establece el Decreto-ley Nº 6769/1958, Ley Orgánica de las Municipalidades de la Provincia de Buenos Aires, cada distrito busque la mejor forma de aplicar la norma nacional en sus comunidades, teniendo en cuenta sus recursos naturales, sus problemáticas ambientales y las necesidades de desarrollo en un contexto local de justicia socio-ambiental.

La discusión medioambiental está cada vez más presente en la vida pública de los grandes centros urbanos metropolitanos. Una política ambiental eficiente es una necesidad indiscutible, que se instaló en la discusión pública de los asuntos municipales, promovida por la creciente contaminación del aire, agua y suelos en las ciudades, en conjunto con la incertidumbre sobre cómo el Cambio Climático puede afectar la calidad de vida de los ciudadanos.

Debemos mantener una mirada integrada al problema global, sin dejar de atender los problemas urgentes y directos del medio ambiente en nuestras ciudades.

 

###### _Leonardo Güi: Abogado. Docente de Derechos Humanos desde la Perspectiva Internacional, Facultad de Ciencias Jurídicas y Políticas, sede San Isidro, extensión áulica Tigre, Universidad de Ciencias Empresariales y Sociales._