+++
bg_image = "/images/incendios-de-corrientes.jpg"
concejales = "Leonardo Güi"
date = 2022-02-24T03:00:00Z
description = ""
image = "/images/incendios-de-corrientes.jpg"
title = "CORRIENTES: LA POLÍTICA AMBIENTAL FRENTE AL DESASTRE ECOLÓGICO"

+++

##### **Debemos promover una ciudadanía preocupada por el medio ambiente y una conducción política sensible al tesoro que significa la biodiversidad de nuestro país**

La provincia de Corrientes está atravesando una crisis ambiental sin precedentes. Las diversas fuentes periodísticas indican que aproximadamente un 9% de la superficie de la provincia se encuentra alcanzada por el fuego, abarcando una extensión de casi 800.000 hectáreas, un número aterrador, especialmente si tenemos en cuenta que cada hectárea es un ecosistema que se destruye, una fuente de vida arrasada por el fuego y la imprudencia humana.

Corrientes es una provincia con una biodiversidad única. Los Esteros del Iberá, homónimo tesoro natural correntino, es un gigantesco humedal que es el ambiente natural de una flora y fauna rica y diversa, conformando además uno de los humedales más grandes del mundo. Calificado como sitio Ramsar (humedal de importancia internacional), este ecosistema alberga también al Parque Nacional Iberá, creado por la Ley nacional N° 27.481 del 2018, que posee una superficie de 195.094 hectáreas. El parque se enmarca dentro de la Reserva Natural del Iberá, que fue creada en 1983, abarcando 1.300.000 hectáreas.

Este entramado administrativo, además de los esfuerzos provinciales y municipales de la citada provincia por proteger su riqueza natural, demuestra la importancia e interés del Estado Argentino en todos sus niveles por preservar y proteger esta riqueza biológica, tanto para las generaciones actuales como para las venideras. Sin embargo, ¿cómo llegamos al catastrófico escenario actual?

A la combinación de un escenario climático complejo, como una sequía extraordinaria muy probablemente relacionada a la crisis que en todo el planeta registra fenómenos extremos, sumamos la imprudencia y negligencia humana sobre el manejo del fuego, un cóctel explosivo que solo necesitaba de una chispa para ser detonado.

Diversas son las causas que pueden citarse para señalar cómo surgen estos incendios. La quema clandestina como método tradicional de desmalezado y renovación de pastizales es una de las razones. Esta práctica tradicional (e imprudente) transformó en una bomba de tiempo buena parte de la provincia, además de otros usos equivocados y una cultura peligrosa respecto al manejo del fuego.

Asumir el daño ambiental que está sufriendo la hermana provincia significa reconocer la pérdida que ocasionan los incendios a la riqueza natural y cultural de los argentinos. Un daño de enorme importancia al patrimonio natural nacional. También la crisis pone en evidencia las dificultades para articular políticas tanto preventivas como paliativas entre los gobiernos municipales, provinciales y la administración federal.

El respeto a las autonomías locales, tanto provinciales como municipales, puede coexistir con redes federales que actúen rápido ante este tipo de catástrofe, ya que la respuesta local se demuestra insuficiente. Es extremadamente complejo que una provincia pueda contener con recursos propios los cientos de miles de hectáreas incendiadas a un ritmo acelerado.

A la dificultad climática y social se la debe enfrentar con políticas activas y coherentes en materia ambiental. La República Argentina cuenta con dos leyes claves en este campo: la Ley Nº 27621 de Educación Ambiental Integral (EAI), con el objetivo de establecer el derecho a la educación ambiental integral como una política pública nacional, y la Ley Nº 27592, también conocida como “Ley Yolanda”, que establece la formación integral en medio ambient: “para todas las personas que se desempeñen en la función pública en todos sus niveles y jerarquías”. Estas leyes tienen una finalidad capacitadora, tanto en los cargos públicos como en toda la ciudadanía por medio de la educación.

La legislación argentina también cuenta con la Ley Nº 27604, modificatoria de la Ley Nº 26815, de manejo del fuego. En su artículo 22 bis prohíbe por un plazo de 60 años “realizar modificaciones en el uso y destino que dichas superficies poseían con anterioridad al incendio”, específicamente en “caso de incendios, sean estos provocados o accidentales, que quemen vegetación viva o muerta, en bosques nativos o implantados, así como en áreas naturales protegidas debidamente reconocidas y humedales”.

La respuesta no puede basarse únicamente en la legislación. La ley es un requisito necesario, pero su cumplimiento debe ser una obligación ética frente a lo que está sucediendo. Nos educamos no sólo para formarnos como ciudadanos, sino para cuidar nuestros bienes colectivos, el aire que respiramos, el agua que bebemos, el equilibrio biológico que sostiene la vida humana como la conocemos. Una autopreservación necesaria, de la mano de la Ley, pero también de la conciencia, de la indignación, de las prioridades de subsistencia y de un crecimiento sostenible.

La problemática ambiental debe ser estratégica y central frente a un escenario tan complejo, la política ambiental debe ser federal, transversal e integrada, del Litoral a la Patagonia.

Asistimos impávidos al desastre ecológico de la provincia de Corrientes y esperamos que pronto termine esta pesadilla. Pero no debemos quedarnos quietos, sino que debemos crear y promover una ciudadanía preocupada por el medio ambiente, y una conducción política sensible al tesoro que significa la biodiversidad de nuestro país. Las futuras generaciones lo agradecerán.

###### Autor: Leonardo Güi