+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
bio = ""
cargop = "Concejal"
control = "juntos"
description = ""
image = "/images/xime-p.jpeg"
rol = "concejal"
rolp = "concejal"
tipo = "juntos"
title = "Ximena Pereyra"
type = ""
[[cargos]]
cargo = "Concejal"
[[contact]]
icon = "ti-facebook"
link = "https://www.facebook.com/ximepereyraok"
name = "Facebook"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/ximepereyraok/"
name = "Instagram"

+++
#### Información