+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
bio = ""
cargop = "Concejal"
control = "juntos"
description = ""
image = "/images/whatsapp-image-2021-12-20-at-12-52-41.jpeg"
rol = "concejal"
rolp = "concejal"
tipo = "juntos"
title = "Marcela Césare"
type = "teacher"
[[cargos]]
cargo = "Concejal"
[[contact]]
icon = "ti-facebook"
link = "https://www.facebook.com/Marcelacesareok"
name = "Facebook"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/marcelacesare/"
name = "Instagram"

+++
### Información

Curso los estudios secundarios en el Instituto José Manuela Estrada de Don Torcuato, donde también realizo el Profesorado en Ciencias Jurídico Contable.

Sus primeros trabajos como docente secundaria fueron en el Instituto San Marcelo, la Escuela Media. En el año 2007 tuvo la posibilidad de ingresar como ayudante de una Catedra en la carrera de la Licenciatura en Organización Industrial, y una catedra en la Tecnicatura Universitaria en Programación, las cuales está dictando actualmente.

Realizo el capitulo de un libro de microemprendimientos, con una colega.

Realizo la carrera de grado para obtener el título de Lic. en Administración de empresas.

Fue Coordinara Pedagógica el Anexo Universitario de UTN en José C Paz.

Siempre incursionando en cursos y capacitaciones, realizo también una Diplomatura en Coaching ontológica, con Tesina pendiente.

Se dedico en forma paralela a emprendimientos familiares, en diversos rubros, realizando Gestión empresarial, pasando por funciones desde administración, RRHH, logística entre otras.

La política siempre fue parte en su vida, los temas de economía y gestión van de la mano. Observadora de la realidad política económica, es que decidió participar en forma mas activa como voluntaria en el área social.

Siempre pensé que se puede debatir, y las ideas pueden ser distintas, pero la primera responsabilidad del Estado es coordinar y guiar a la sociedad para que puedan por desenvolverse con sus derechos y obligaciones, siempre apuntando al bien común, y generando trabajo. Donde a ningún ciudadano argentino puede faltarle el derecho a la salud, educación y Justicia.