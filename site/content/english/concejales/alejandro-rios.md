+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
bio = ""
cargop = ""
control = "frente"
description = ""
image = "/images/alejandro-rios.jpeg"
rol = "concejal"
rolp = "concejal"
tipo = "frente"
title = "Alejandro Ríos"
type = "teacher"
[[cargos]]
cargo = "Concejal"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/alejandroriospf/"
name = "Instagram"

+++
#### Información

Alejandro Ramón Ríos nació el 14 de diciembre de 1986 en San Fernando. Es oriundo de Troncos del Talar en Tigre y actualmente reside Benavidez. Está casado con Micaela, con quien tiene dos hijos, Joaquín de 11 años y Benicio de 7.

Arrancó su labor comunitaria junto a movimientos sociales en el 2010. Acciones que sigue generando hasta el día de hoy.

Fue Director Coordinador del Puerto de frutos entre los años 2014 y 2019, para luego asumir como Director General de la misma entidad del 2019 al 2021.