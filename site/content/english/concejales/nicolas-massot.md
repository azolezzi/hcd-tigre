+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
bio = ""
cargop = "Concejal"
control = "juntos"
description = ""
image = "/images/nico-m.jpg"
rol = "concejal"
rolp = "concejal"
tipo = "juntos"
title = "Nicolás Massot"
type = "teacher"
[[cargos]]
cargo = "Concejal"
[[contact]]
icon = "ti-facebook"
link = "https://www.facebook.com/nico.massot"
name = "Facebook"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/nico.massot/"
name = "Instagram"

+++
#### Información

Nicolás Massot es licenciado en Economía con orientación en Historia de la Universidad Torcuato di Tella y Université de Paris IX (Dauphine). En 2019, fue becario y profesor invitado en la Universidad de Yale por el programa Yale World Fellows. 

Entre 2015 y 2019 fue diputado nacional y se desempeñó como jefe del bloque PRO durante los 4 años de su mandato. 

Es consultor de empresas y director del Banco Ciudad de Buenos Aires. Anteriormente trabajó en finanzas corporativas en la industria naviera.

En diciembre de 2021 asumió como concejal del Partido de Tigre por el Bloque de Juntos por el Cambio. En la actualidad, preside la comisión de Legislación y forma parte de las comisiones de Hacienda, Urbanismo y Seguridad. 

Está casado y vive en Don Torcuato junto a su mujer y sus dos hijos.