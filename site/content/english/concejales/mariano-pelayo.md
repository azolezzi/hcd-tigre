---
title: Mariano Pelayo
control: juntos
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/pelayo-1.jpeg"
cargos:
- cargo: Concejal
rol: concejal
rolp: concejal
tipo: juntos
partido:
- name: Juntos por el Cambio
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/juntos-por-el-cambio
course: Juntos por el Cambio
bio: Mariano Pelayo
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/marianojpelayo
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/marianopelayo
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/mariano_pelayo

---
### Información Personal

Soy un convencido que las verdaderas transformaciones se hacen desde la política, y mientras más personas se involucren, mejores resultados vamos a lograr como sociedad. Necesitamos establecer políticas públicas sólidas, que perduren en el tiempo y trasciendan a los gobiernos.

Creo en el desarrollo productivo y el sector privado como principal motor para un crecimiento sostenido. Desde el Estado debemos facilitar y otorgar las condiciones necesarias para que el sector privado se desarrolle con mayor libertad. Es una obligación nuestra bajar toda presión impositiva que perjudique el crecimiento.