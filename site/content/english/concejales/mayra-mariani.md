---
title: "Mayra Mariani"
draft: false
control: "frente"
# page title background image
bg_image: "images/backgrounds/Recinto2-Recuperado.jpg"
# teacher portrait
image: "images/concejales/Mayra-Mariani.jpg"
# cargos
cargos:
  - cargo : "Concejal"

rol: "concejal"
rolp: "concejal"
tipo: "frente"
#Partido
partido:
  - name : "Frente de Todos"
  - linkpartido : "www.smokmedia.gitlab.io/assets/hcd-tigre/frente-de-todos"
#course
course: "Juntos por el Cambio"
# biography
bio: ""
# interest
interest: [""]
# contact info
contact:
  # contact item loop
  - name : "Email"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "https://mailto:"

  # contact item loop
  - name : "Facebook"
    icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
    link : "https://www.facebook.com/mayralorenamariani"

  # contact item loop
  - name : "Twitter"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "https://www.twitter.com/mayramariani"

  # contact item loop
  - name : "Instagram"
    icon : "ti-instagram" # icon pack : https://themify.me/themify-icons
    link : "https://www.instagram.com/mayra_mariani"

# type
type: "concejales"
---

### Informacion Personal

