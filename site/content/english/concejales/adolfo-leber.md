+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
bio = ""
cargop = "Concejal"
control = "juntos"
description = ""
image = "/images/fito-leber.jpeg"
rol = "concejal"
rolp = "concejal"
tipo = "juntos"
title = "Adolfo Leber"
type = "teacher"
[[cargos]]
cargo = "Concejal"
[[contact]]
icon = "ti-facebook"
link = "https://www.facebook.com/fito.leber"
name = "Facebook"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/fitoleber/"
name = "Instagram"

+++
#### Información

Contador Público y Licenciado en Administración de Empresas, quien desde sus comienzos se desempeñó en el ámbito privado, en el ramo de la consultoría.

A partir del 2009 decide regresar al estudio contable familiar “Leber Asociados” donde hoy, continúa brindando servicios.

El 10 de Diciembre del 2021 asume como Concejal, por el espacio “Juntos” en busca de participar más activamente en la vida política de Tigre.