---
title: Ana María Ramos Fernándes Costa
control: juntos
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/ana-maria-fernandez-costa-1-1.jpeg"
cargos: []
cargop: Presidenta del Bloque de Juntos
rol: concejal
rolp: Presidente
tipo: juntos
partido:
- name: Juntos por el Cambio
- linkpartido: "/assets/hcd-tigre/juntos-por-el-cambio/"
course: Juntos por el Cambio
bio: Ana maría..
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/

---
### Información Personal

Nació en Portugal y de pequeña vino a la Argentina, para radicarse en la ciudad de General Pacheco, dónde aún sigue viviendo.

Es docente, madre de 4 hijos.

Su vocación es la docencia y dedicó toda su vida al desarrollo de actividades en diferentes escuelas de la gestión pública y privada. Es catequista y forma parte de la comunidad de la parroquia Inmaculada Concepción.