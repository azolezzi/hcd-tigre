---
title: Maximiliano Picco
control: juntos
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/whatsapp-image-2020-12-09-at-17-25-04.jpeg"
cargos:
- cargo: Concejal
rol: concejal
rolp: Vice
tipo: juntos
partido:
- name: Juntos por el Cambio
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/juntos-por-el-cambio
course: Juntos por el Cambio
bio: "Nació el 19 de diciembre de 1985 en la ciudad de Don Torcuato, partido de Tigre.
  \ \nEs Licenciado en Administración de Empresas, recibido en la Universidad de Lujan
  en la sede de San Miguel.\nEn el año 2008, formo parte de la juventud política del
  PRO, formando Juventud Cambiemos para el Partido de Tigre\nInicio su trabajo en
  política como asesor en el Honorable Concejo Deliberante de Tigre, desde el 2009
  al 2011\nEn 2015 fue candidato a concejal por el bloque de Cambiemos en Tigre.\nEn
  abril de 2016 asumió como Jefe de Agencia de PAMI Tigre, donde se desempeñó hasta
  diciembre de 2019\nEn diciembre de 2019, asumió como concejal, en el Partido de
  Tigre, por el Bloque de Juntos por el Cambio.  En la actualidad, preside la camisión
  de Seguridad, y forma parte de las comisiones de Obra Pública y Urbanismo."
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/maxipicco
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/maximiliano.picco
cargop: Vice Presidente del Bloque Juntos

---
### Información Personal

Maximiliano Picco nació en Don Torcuato el 19 de diciembre de 1985.

Edad 37 años.

Curso su vida hasta el momento siempre en la ciudad de Don Torcuato.

Licenciado en Administración, se recibió en 2018 en Universidad de Lujan, sede San Miguel

Desde los 23 años, en el año 2008 se integró a la juventud política del PRO, empezando a formar equipo de jóvenes voluntarios para desarrollo de actividades dentro del Partido de Tigre, empezando de esta forma a formar equipos de Juventud de Tigre.

La primera labor política fue como asesor en el Honorable Concejo Deliberante de Tigre, desde el 2009 -2011.

En 2015 fue candidato a concejal para el partido de Tigre por Cambiemos PRO

En abril de 2016, empezó su labor como Jefe de Agencia de PAMI Tigre, donde se desempeñó hasta el 2019.

En diciembre de 2019, asumió como concejal, por el Bloque de Juntos por el Cambio, en el Concejo Deliberante de Tigre. En la actualidad, preside la comisión de Seguridad y forma parte de las comisiones de Obra Publica y Urbanismo.

Desde siempre, teniendo como objetivo, trabajar para mejorar la calidad de vida de los vecinos de Tigre, así como el cuidado de los recursos naturales que tiene este distrito.