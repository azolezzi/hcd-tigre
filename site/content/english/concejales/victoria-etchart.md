+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
bio = ""
cargop = ""
control = "frente"
description = ""
image = "/images/vicky-etchart.jpeg"
rol = "concejal"
rolp = "concejal"
tipo = "frente"
title = "Victoria Etchart"
type = "teacher"
[[cargos]]
cargo = "Concejal"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/victoriaetchart/"
name = "Instagram"

+++
#### Información