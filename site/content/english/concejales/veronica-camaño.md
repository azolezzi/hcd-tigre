---
title: Verónica Caamaño
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/v-caamano.jpeg"
cargos:
- cargo: Concejal
rol: concejal
rolp: Vice
tipo: frente
partido:
- name: Frente de Todos
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/frente-de-todos
course: Juntos por el Cambio
bio: "Concejala Ad Honorem del Frente Renovador (Bloque Frente de Todos).\n \nPresidenta
  de la Comisión de Derechos Humanos del Honorable Concejo Deliberante de Tigre.\n\nVicepresidenta
  del bloque FDT de Tigre."
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/caa_vero
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/veronicacaamano_
cargop: Vice Presidenta del Bloque Frente de Todos

---
### Información Personal