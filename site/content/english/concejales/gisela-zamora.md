---
title: Gisela Zamora
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/zamora.jpg"
cargos:
- cargo: Concejal
rol: concejal
rolp: Presidente
tipo: frente
partido:
- name: Frente de Todos
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/frente-de-todos
course: Juntos por el Cambio
bio: ''
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/giselalzamora
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/giselazamoraok
cargop: Presidenta del Bloque Frente de Todos

---
### Informacion Personal