---
title: Rodrigo Molinos
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/r-molinos.jpeg"
cargop: Concejal
rol: concejal
rolp: Presidente
tipo: frente
partido:
- name: Frente de Todos
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/frente-de-todos
course: Juntos por el Cambio
bio: ''
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/rodrigomolinos
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/rodrigojuanmolinos

---
### Información Personal

Rodrigo Molinos, es vecino de Tigre Centro, abogado y concejal desde fines del 2007. Comenzó su carrera dentro de la administración pública como jefe de la delegación Tigre del Registro Provincial de las Personas, encabezando en numerosas oportunidades operativos de documentación. Asimismo, fue Presidente del Club Atlético Tigre, durante 2010-2016, en el cual durante su gestión ha jerarquizado al Club de la Zona Norte. Actualmente se desempeña como Presidente del Bloque del Frente de Todos y ejerce en el ámbito privado a cargo de su propio estudio de abogados.

Como jefe de bloque, tiene como principal función la de coordinar la agenda legislativa, a través del dialogo entre los bloques políticos, las autoridades del cuerpo y el departamento ejecutivo. Durante su mandato ha sido el principal orador en normativas claves como la aprobación de los presupuestos municipales y sus respectivas rendiciones de cuentas, la presentación y acompañamiento de proyectos legislativos cuyo objetivo máximo es mejorar la calidad de vida del vecino como así también, en la defensa de los intereses del municipio.