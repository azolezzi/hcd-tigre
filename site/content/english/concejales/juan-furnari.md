---
title: Juan Furnari
control: juntos
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/furnari.jpeg"
cargop: Secretario del Bloque Juntos
rol: concejal
rolp: Secretario
tipo: juntos
partido:
- name: Juntos por el Cambio
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/juntos-por-el-cambio
course: Juntos por el Cambio
bio: Juan Furnari
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/juanfurnari
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/juanmafurnari

---
### Información Personal

Juan María Furnari es abogado, especializado en derecho ambiental y políticas públicas.

Trabajó en la gestión de servicios públicos para la Ciudad de Buenos Aires hasta el año 2015.

Fue director nacional de empleo entre enero del 2016 y diciembre del 2018. 

Desde 2019 se desempeña como concejal del partido de Tigre en el Bloque de Juntos, donde preside la comisión de transporte e integra la de urbanismo y legislación.