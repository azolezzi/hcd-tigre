---
title: Sofía Bravo Adamoli
control: juntos
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: images/concejales/SOFIA-BRAVO.jpeg
cargos:
- cargo: Vice Presidenta 2° del Honorable Concejo Deliberante de Tigre
rol: Vice2
rolp: concejal
tipo: juntos
partido:
- name: Juntos por el Cambio
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/juntos-por-el-cambio
course: Juntos por el Cambio
bio: Nació en 1987 en San Juan. Es abogada recibida de la Universidad Católica Argentina.
  Desarrollo parte de su vida profesional en YPF, la Corte Suprema de Justicia de
  la Nación y OPISU en el Barrio Garrote/Almirante Brown. Integra las comisiones de
  Bienestar Social, Salud Publica, Prevención y lucha contra las adicciones; Turismo,
  Recreación, Deportes, Islas y Prevención del Medio ambiente; y Legislación, interpretación
  y reglamento.
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/sofiabravook
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/sofiabravo.ok
cargop: Concejal

---
### Información Personal

Estudió abogacía en la Universidad Católica Argentina donde se recibió en febrero de 2011. Desde Mayo de 2010 hasta noviembre de 2012, trabajó en la Dirección de servicios jurídicos de YPF, donde se desempeñó primero en Departamento de Gas Natural, GLP, GNL y Energías renovables y luego realizó tareas de apoyo en la coordinación del directorio de YPF. A partir de 2013 y hasta febrero de 2018 se desempeñó en la Corte Suprema de Justicia de la Nación como secretaria privada del Ministro Ricardo Lorenzetti, por ese entonces Presidente de la CSJN. Además, fue coordinadora del proyecto Gobierno Abierto, Foro de Políticas de Estado. En marzo de 2018 se desempeñó en la dirección de Ceremonial y protocolo de la CSJN. Desde febrero de 2019 se desempeñó como coordinadora territorial de OPISU (Organismo Provincial de Integración social y urbana) en el Barrio Garrote/ Almirante Brown de Tigre. En diciembre de 2019 asumió como concejal en el Distrito de Tigre por Juntos por el Cambio. Integra las comisiones de Bienestar social, Salud Pública, Prevención y Lucha contra las Adicciones; Turismo, Recreación, Deportes, Islas y Protección del Medio Ambiente; y Legislación, Interpretación y Reglamento.