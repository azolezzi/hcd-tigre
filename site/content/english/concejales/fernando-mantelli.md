---
title: Fernando Mantelli
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/f-mantelli.jpg"
cargop: ''
rol: Vice1
rolp: Vice
tipo: frente
partido:
- name: Frente de Todos
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/frente-de-todos
course: Juntos por el Cambio
bio: ''
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/MantelliFOficia
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/fernando.d.mantelli
cargos:
- cargo: Vice Presidente 1° del Honorable Concejo Deliberante de Tigre

---
### Información Personal