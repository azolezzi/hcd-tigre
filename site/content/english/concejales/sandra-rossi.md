---
title: Sandra Rossi
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: images/concejales/Sandra-Rossi.jpg
cargos:
- cargo: Prosecretaria del Honorable Concejo Deliberante de Tigre
rol: prosec
rolp: concejal
tipo: frente
partido:
- name: Frente de Todos
- linkpartido: www.smokmedia.gitlab.io/assets/hcd-tigre/frente-de-todos
course: Juntos por el Cambio
bio: ''
interest:
- ''
contact:
- name: Email
  icon: ti-email
  link: 'https://mailto:'
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/sberossi
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/rossisandrab

---
### Información Personal