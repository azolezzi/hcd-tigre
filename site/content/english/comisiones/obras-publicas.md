---
title: Obras Públicas
bg_image: images/Comisiones/obra-publica-fondo.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Francisco Fernández
- puesto: 'Vicepresidente:'
  nombre: Maximiliano Picco
- puesto: 'Secretario:'
  nombre: Rodrigo Álvarez
- puesto: 'Vocal:'
  nombre: Alejandro Ríos
- puesto: 'Vocal:'
  nombre: Lisandro Lanzón
- puesto: 'Vocal:'
  nombre: Adolfo Leber
- puesto: 'Vocal:'
  nombre: Sofía Bravo Adamoli

---
##### Día de Reunión: Miércoles, 11:30 am.

### Funciones

a. Todo asunto referente a la obra pública municipal que por aplicación de la Ley Orgánica de las Municipalidades sea competencia del HCD.


b. Todo asunto referente a la aplicación de la Ordenanza General 165, respecto a redes de agua potable, cloacas, alumbrado público y gas natural.


c. El tránsito de vehículos públicos y privados en calles y caminos de jurisdicción municipal, el estacionamiento en espacios públicos, operaciones de cargas y descargas, señalizaciones, en concordancia con las normas establecidas por el Código de Tránsito de la Provincia de Buenos Aires.


d. Cercos y veredas, control de pesas y medidas, incendios, inundaciones, derrumbes, demoliciones, delineaciones y niveles, obras hidráulicas y de riego, nomenclaturas y numeraciones, condominios de muros y cercos, evacuación del humo y aguas servidas.


e. Conservación de calles y caminos, puentes, túneles, plazas, paseos públicos, monumentos y alumbrado público.