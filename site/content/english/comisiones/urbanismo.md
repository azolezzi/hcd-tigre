---
title: Urbanismo
date: '2019-07-06T15:27:17.000+06:00'
bg_image: images/Comisiones/urbanismo-fondo.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Mariano Pelayo
- puesto: 'Vicepresidente:'
  nombre: Rodrigo Alvarez
- puesto: 'Secretario:'
  nombre: Juan Furnari
- puesto: 'Vocal:'
  nombre: Maximiliano Picco
- puesto: 'Vocal:'
  nombre: Francisco Fernández
- puesto: 'Vocal:'
  nombre: Mayra Mariani
- puesto: 'Vocal:'
  nombre: Gisela Zamora
- puesto: 'Vocal:'
  nombre: Nicolás Massot

---
##### Día de Reunión: Miércoles, 10:30 am.

### Funciones

a. Los proyectos o asuntos que se relacionen con la concesión, autorización, reglamentación de obras arquitectónicas, de urbanismo y de ornato; trazado y ejecución de calles y avenidas.


b. Todo lo referido al dominio público y/o privado municipal; limitación del dominio privado, expropiaciones.


c. Subdivisiones del suelo, régimen de propiedad horizontal.


d. Construcción y conservación de cementerios, teatros, templos y demás edificios destinados   a reuniones públicas.


e. Todo lo relacionado con el Planeamiento Urbano, el código de Zonificación del Partido de 
Tigre, la Ley 8912 (Uso del Suelo) y el Código Civil.


f. Todo lo referido al régimen de radicación y funcionamiento industrial, en concordancia con las leyes nacionales y provinciales vigentes.