---
title: Turismo, Deporte y Medio Ambiente
bg_image: images/Comisiones/turismo-fondo.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Mayra Mariani
- puesto: 'Vicepresidente:'
  nombre: Adolfo Leber
- puesto: 'Secretario:'
  nombre: Francisco Fernández
- puesto: 'Vocal:'
  nombre: Florencia Mosqueda
- puesto: 'Vocal:'
  nombre: Sofía Bravo Adamoli
- puesto: 'Vocal:'
  nombre: Lisandro Lanzón
- puesto: 'Vocal:'
  nombre: Ximena Pereyra

---
##### Día de Reunión: Miércoles, 13:00 pm.

### Funciones

a. Todo lo relativo a la promoción y explotación del turismo, y sus medios de implementación, incluyendo la reglamentación del funcionamiento y explotación de la industria hotelera.


b. Todo lo vinculado con las actividades deportivas y de recreación.


c. Todo lo relacionado con las Islas y las actividades propias, presentes y futuras de dicha zona, en todos sus aspectos.


d. La preservación de los sistemas ecológicos; forestaciones.


e. La preservación de la contaminación de los tres elementos esenciales: aire, agua y tierra.
Evitar el uso indiscriminado de los recursos naturales.