---
title: Transporte
date: '2019-07-06T15:27:17.000+06:00'
bg_image: images/Comisiones/transporte-fondo.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Juan Furnari
- puesto: 'Vicepresidente:'
  nombre: Rodrigo Molinos
- puesto: 'Secretario:'
  nombre: Ana María Fernándes Costa
- puesto: 'Vocal:'
  nombre: Mariano Pelayo
- puesto: 'Vocal:'
  nombre: Verónica Caamaño
- puesto: 'Vocal:'
  nombre: Victoria Etchart
- puesto: 'Vocal:'
  nombre: Alejandro Ríos

---
##### Día de Reunión: Miércoles, 13:30 pm.

### Funciones

a. Dictaminar sobre todo asunto o proyecto relativo al transporte en general.


b.  Todo lo relativo respecto al transporte colectivo de pasajeros (líneas comunales), habilitación, fijación de recorridos, paradas, tarifas, conservación y mantenimiento de los vehículos.


c. Reglamentación del funcionamiento del transporte escolar.


d. Reglamentación del funcionamiento de vehículos de alquiler (taxis) y de agencias de remises, habilitación, paradas, tarifas