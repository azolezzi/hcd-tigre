---
title: Derechos Humanos
date: '2019-07-06T15:27:17.000+06:00'
bg_image: images/Comisiones/ddhh-fondo.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Verónica Caamaño
- puesto: 'Vicepresidente:'
  nombre: Marcela Césare
- puesto: 'Secretario:'
  nombre: Fernando Mantelli
- puesto: 'Vocal:'
  nombre: Sandra Rossi
- puesto: 'Vocal:'
  nombre: Ximena Pereyra
- puesto: 'Vocal:'
  nombre: Josefina Ponde
- puesto: 'Vocal:'
  nombre: Lisandro Lanzón

---
##### Día de Reunión: Miércoles, 11:00 am.

### Funciones

a. Protección y promoción de las libertades, reivindicaciones y facultades propias de cada individuo por el sólo hecho de pertenecer a la raza humana.


b. Difusión y auspicio de programas y campañas sobre la defensa de los derechos fundamentales; toda iniciativa que procure avances concretos, profundice y fortalezca la conciencia de la comunidad, sus organizaciones y sus dirigentes en la temática de la libertad idéntica en derechos y dignidad; todo hecho o acto de abuso, maltrato, discriminación, marginación, exclusión, denegación de justicia o derechos básicos que afecten a los ciudadanos individual o colectivamente, ejercidos por la fuerza pública u otro poder que actúe sin justificación o ilegalmente.


c. La adecuación de la legislación que es competencia del Honorable Cuerpo respaldad en la normativa de la legislación nacional e internacional al respecto.