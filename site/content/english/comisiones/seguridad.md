---
title: Seguridad
bg_image: images/Comisiones/seguridad-tigre-fondo2.jpg
description: Conocé más sobre esta comisión
download_link: "#"
categoriess:
- Seguridad
imagen: images/backgrounds/page-title.jpg
puesto:
- puesto: 'Presidente:'
  nombre: Maximiliano Picco
- puesto: 'Vicepresidente:'
  nombre: Mayra Mariani
- puesto: 'Secretario:'
  nombre: Nicolás Massot
- puesto: 'Vocal:'
  nombre: Lisandro Lanzón
- puesto: 'Vocal:'
  nombre: Micaela Ferraro
- puesto: 'Vocal:'
  nombre: Florencia Mosqueda
- puesto: 'Vocal:'
  nombre: Fernando Mantelli

---
##### Día de Reunión: Miércoles, 12:30 pm.

### Funciones

a. El tratamiento de todos los temas relativos a la seguridad en general, comprendidos en el marco de la “Ley Provincial de Seguridad Pública” y afines.


b. Todo lo relacionado a los servicios que brindan los organismos policiales de competencia en el Distrito.


c. Los temas vinculados a las empresas de seguridad de carácter privado, en concordancia con las leyes y disposiciones nacionales y provinciales que regulan su actividad.