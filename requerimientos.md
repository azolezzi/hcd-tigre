- [ ] !Importante. Requerimiento de contenidos de cada apartado de la pagina.
       
1.      Home .
      
2.      Noticias (Blog)(Listo falta contendio)
3.      Vivo (Necesitamos saber plataforma y como quieren que se muestren sus anteriores transmiciones)
4.      Consejo (Reducimos en 2 apartados ya que en consejales hay un filtro de bloques y comisiones)
                Autoridades
                Consejales (Listo el formato, falta imagen)
5.      Secciones
                Orden del dia
                Actas aprobadas
                Archivo Audiovisuales
6.      Part. Ciudadana
                Atencion al vecino (Colocamos un formulario en el cual el usuario coloca su informacion. Falta contenido textual explicativo)
                Iniciativa Ciudadana
                Transparencia
                Observaciones

(Se necesita informacion sobre el contenido que contendra y de que forma. Ejemplo, En la seccion consejales, el mismo muestra todos los consejales actualmente, el cual luego se podra filtrar por bloques partidarios. La seccion noticias contendra las ultimas noticias respecto al consejo el cual contendra filtros por TAGS o CATEGORIAS)

- [ ] Imagenes a colocar con detalle en que apartado de ser necesario.
- [ ] Definir colores principales y secundarios de la pagina.
- [ ] Logo y miniatura.
- [ ] Logos de 3ro (Ejemplo municipalidad de tigre, gobierno de la provincia etc.) (De ser necesario)
- [ ] Fotos de los actuales consejales, con lujos de detalles respecto a sus redes sociales de ser requeridos. (Ver si implementar o no redes sociales de los consejales). Informacion de cada uno, nombre completo, bloque al que corresponde, y puesto que ocupa (Presidente, viceprecidente tanto del bloque como del mismo consejo).
- [ ] Informacion de integridad del consejo. Cantidad de bloques actualmente con la respectiva cantidad de integrantes del mismo. Conformidad de las autoridades del consejo y toda la informacion necesaria que se pueda brindar.
- [ ] Informacion sobre ubicacion del consejo (Direccion), telefono, mail y cualquier otro medio de informacion que se quiera dar al publico para la comunicacion entre las dos partes.
- [ ] Configuracion del hosting y Dominio
- [ ] RRDD del consejo para colocar con sus iconos correspondiente.
- [ ] Tipo de archivo de carga de las seciones, actas aprobadas y archivoaudiovisuales. Y como se procedera a su carga el dia de mañana. (Portal a ultilizar.)